FROM circleci/node:8.11.3-browsers

RUN curl https://install.meteor.com | /bin/sh

ADD ./scripts/* /usr/local/bin/

USER root
RUN chmod +x /usr/local/bin/*.sh

USER circleci
