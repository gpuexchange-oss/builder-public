#!/usr/bin/env bash

set -xe

meteor npm run dev &

until $(curl --output /dev/null --silent --head --fail http://localhost:3000); do
    printf '.'
    sleep 5
done

meteor npm run test-e2e
